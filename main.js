const https = require('http');

function parseCall(url, parseFunc) {
    https.get(url, r => {
        let data = '';
        r.on('data', q => data += q);
        r.on('end', q => parseFunc(data));
    });
}

// uses a regex to find all <a></a> elements in a given text.
function extractAnchors(text) {
    let matches = text.match(/<a[^>]*>([^<]+)<\/a>/g);
    return matches;
}

// uses a regex match to extract the link from a given anchor element.
// the function makes an assumption that all urls are local on the domain we are traversing.
function extractLink(link) {
    return link.match(/(["'])(?:(?=(\\?))\2.)*?\1/g)[0].replace(/\"/g, "");
}

function notVisited(url) {
    for (let val of pageData) {
        if (val.url == url)
            return false;
    }
    return true;
}

function processData() {
    console.log(pageData);
}

var pageData = [];
function traverseQueue(queue, rootUrl) {
    let currentUrl = queue.splice(0, 1)[0];
    parseCall(currentUrl, (data) => {
        let links = extractAnchors(data);
        if (links) {
            links.forEach(link => {
                let url = extractLink(link);
                url = rootUrl.concat(url);
                if (notVisited(url)) {
                    queue.push(url);
                }
            });
        }
        pageData.push({
            "url": currentUrl,
            "data": "" // change this to add the data attribute to add the data to the pageData object.
        });
        if (queue.length == 0) {
            // we are done traversing the website, let's process the data
            processData();
        } else {
            traverseQueue(queue, rootUrl);
        }
    });

}

traverseQueue(['http://polygonic.io/'], 'http://polygonic.io/');
